package com.training.rest.core;

import io.restassured.http.ContentType;

public interface Properties {

    String APP_BASE_URL = "https://barrigarest.wcaquino.me/";
    Integer APP_PORT = 443;
    String APP_BASE_PATH = "";
    ContentType APP_CONTENT_TYPE = ContentType.JSON;
    Long APP_MAX_TIMEOUT = 50000L;

}
