package com.training.rest.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transacao {

    private Integer id;

    private String tipo;

    @SerializedName("data_transacao")
    private String dataTransacao;

    @SerializedName("data_pagamento")
    private String dataPagamento;

    private String descricao;
    private String envolvido;
    private double valor;

    @SerializedName("conta_id")
    private Integer contaId;
    private boolean status;

}
