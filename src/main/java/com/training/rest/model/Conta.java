package com.training.rest.model;

public class Conta {

    private String nome;

    public Conta(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
