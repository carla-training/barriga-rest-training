package com.training.rest.suite;

import com.training.rest.core.AbstractTest;
import com.training.rest.model.User;
import com.training.rest.tests.AccountTest;
import com.training.rest.tests.AuthTest;
import com.training.rest.tests.TransactionTest;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static io.restassured.RestAssured.given;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    AccountTest.class,
    TransactionTest.class,
    AuthTest.class
})
public class TestSuite extends AbstractTest {

    @BeforeClass
    public static void login() {
        User user = new User("user@test.com", "user");
        String token = given()
                .body(user)
                .when()
                .post("/signin")
                .then()
                .statusCode(200)
                .extract().path("token");

        RestAssured.requestSpecification.header("Authorization", String.format("JWT %s", token));
    }
}
