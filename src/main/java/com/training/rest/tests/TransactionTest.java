package com.training.rest.tests;

import com.training.rest.core.AbstractTest;
import com.training.rest.model.Transacao;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class TransactionTest extends AbstractTest {

    @Test
    public void shouldAddTransactionWithSuccess() { // movimentação
        int acctId = 248398;
        Transacao transacao = newTransaction();
        transacao.setContaId(acctId);
        Transacao novaTransacao = given()
            .body(transacao)
        .when()
            .post("/transacoes")
        .then()
            .statusCode(201)
            .extract().body().as(Transacao.class);

        Assert.assertEquals(transacao.getContaId(), novaTransacao.getContaId());
        Assert.assertEquals(transacao.getTipo(), novaTransacao.getTipo());
    }


    @Test
    public void shouldValidateTransactionMandatoryFields() { // movimentação
        given()
            .body("{}")
        .when()
            .post("/transacoes")
        .then()
            .statusCode(400)
            .body("$", hasSize(8))
            .body("msg", hasItems(
                            "Data da Movimentação é obrigatório",
                            "Data do pagamento é obrigatório",
                            "Descrição é obrigatório",
                            "Interessado é obrigatório"));

    }

    @Test
    public void shouldNotAddTransactionWithFutureDate() {
        LocalDate today = LocalDate.now().plusDays(5);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String date = today.format(formatter);

        Transacao transacao = newTransaction();
        transacao.setDataTransacao(date);

        given()
            .body(transacao)
        .when()
            .post("/transacoes")
        .then()
            .statusCode(400)
            .body("msg", hasItem("Data da Movimentação deve ser menor ou igual à data atual"));

    }

    @Test
    public void shouldNotDeleteAccountWithTransaction() {
        given()
            .pathParam("id", 249454)
        .when()
            .delete("/contas/{id}")
        .then()
            .statusCode(500)
            .log().all();
    }

    @Test
    public void shouldDeleteTransaction() {
        int transactionId = addTransactionToDelete();

        given()
            .pathParam("id", transactionId)
        .when()
            .delete("/transacoes/{id}")
        .then()
            .statusCode(204);
    }

    public Transacao newTransaction() {
        Transacao transacao = new Transacao();
        transacao.setDataPagamento("19/08/2020");
        transacao.setDataTransacao("19/08/2020");
        transacao.setDescricao("Description");
        transacao.setEnvolvido("Interessado");
        transacao.setStatus(true);
        transacao.setTipo("REC");
        transacao.setValor(1500);

        return transacao;
    }

    public int addTransactionToDelete() {
        Transacao transacao = newTransaction();
        transacao.setContaId(249455);
        return given()
                .body(transacao)
                .when()
                .post("/transacoes")
                .then()
                .statusCode(201)
                .extract().path("id");
    }

}
