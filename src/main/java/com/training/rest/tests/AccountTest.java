package com.training.rest.tests;

import com.training.rest.core.AbstractTest;
import com.training.rest.model.Conta;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class AccountTest extends AbstractTest {

    @Test
    public void shouldAddAcountWithSuccess() {
        String acctName = String.format("Conta %s", System.nanoTime());

        Conta conta = new Conta(acctName);
        given()
                .body(conta)
                .when()
                .post("/contas")
                .then()
                .statusCode(201)
                .body("nome", is(conta.getNome()))
                .extract().path("id");
    }

    @Test
    public void shouldUpdateAcountWithSuccess() {
        int editAcctId = 248395;
        String acctName = String.format("Edit Account %s", System.nanoTime());
        Conta conta = new Conta(acctName);
        given()
                .body(conta)
                .pathParam("id", editAcctId)
                .when()
                .put("/contas/{id}")
                .then()
                .statusCode(200)
                .body("nome", is(conta.getNome()));

    }

    @Test
    public void shouldNotAddDuplicatedAccounts() {
        Conta conta = new Conta("Duplicated Account");
        given()
                .body(conta)
                .when()
                .post("/contas")
                .then()
                .statusCode(400)
                .body("error", is("Já existe uma conta com esse nome!"));
    }

    @Test
    public void shouldSeeAccountBalance() {
        String find = String.format("find{it.conta_id == %s}.saldo", 248397);
        given()
                .when()
                .get("/saldo")
                .then()
                .statusCode(200)
                .body(find, is("6000.00"));
    }

   /* public Integer getAccountIdByName(String name) {
        return RestAssured.get("/contas?name="+name).then().extract().path("id[0]");
    } */
}
